import json

def load(filename):
  with open(filename) as f:
    return json.load(f)

def save(filename, data):
  with open(filename, 'w') as f:
    f.write(pprint(data))

def pprint(data):
  return json.dumps(data, sort_keys = True, indent = 4)
