#!/usr/bin/env python3

import argparse
import sys
from spotify_api import initSpotify

def printArtist(a):
    print(a.id, a.name)

def relatedArtists(api, artists, artist, depth):
    if depth == 0:
        return
    if not hasattr(artist, 'related_artists'):
        artist.related_artists = api.relatedArtists(artist.id).artists
    for a in artist.related_artists:
        aid = a.id
        if aid not in artists:
            artists[aid] = a
            printArtist(a)
    for a in artist.related_artists:
        relatedArtists(api, artists, artists[a.id], depth - 1)

def main():
    parser = argparse.ArgumentParser(description = "get related artists")
    parser.add_argument('artistName', help="artist name")
    parser.add_argument('depth', nargs='?', default=1, type=int, help="related artist depth")
    args = parser.parse_args()

    api = initSpotify()
    artist = api.queryArtist(args.artistName)
    printArtist(artist)
    artists = { artist.id: artist }
    relatedArtists(api, artists, artist, args.depth)

main()
