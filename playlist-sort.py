#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify
from spotifytools import transformPlaylist

def playlistSort(api, playlistId, destPlaylistId, sortKeys, append):
    def parseKey(k):
        dir = k[0:1]
        reverse = False
        if dir == '-':
            reverse = True
            k = k[1:]
        elif dir == '+':
            reverse = False
            k = k[1:]
        return (compile(k, '<sort_key>', 'eval'), reverse)
    def xform(tracks):
        for k in sortKeys:
            print('sorting using key', k)
            (k, reverse) = parseKey(k)
            tracks.sort(key = lambda track: eval(k), reverse = reverse)
        return tracks
    transformPlaylist(api, playlistId, destPlaylistId, xform, append = append)


def main():
    parser = argparse.ArgumentParser(description = "sort playlist")
    parser.add_argument('srcPlaylist', help="source playlist id")
    parser.add_argument('-d', '--dest', dest="destPlaylist", help="destination playlist id")
    parser.add_argument('sortKey', nargs=argparse.REMAINDER, help="sorting key expression")
    parser.add_argument('-a', '--append', action='store_true', help="append to destination playlist")
    args = parser.parse_args()
    if not args.destPlaylist: args.destPlaylist = args.srcPlaylist
    api = initSpotify()
    playlistSort(api, args.srcPlaylist, args.destPlaylist, args.sortKey, args.append)

main()
