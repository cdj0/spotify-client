def artists(t):
    return ', '.join([ a.name for a in t.artists ])

def printTrack(t):
    print(f"{t.id}: {artists(t)} - {t.name}")

def playlistFilter(api, playlistId, destId, trackFilter, **kwargs):
    def f(tracks):
        size = len(tracks)
        tracks = [ t for t in tracks if trackFilter(t) ]
        print(f'Filtered size: {len(tracks)} / {size}')
        return tracks
    transformPlaylist(api, playlistId, destId, f, **kwargs)

def fetchPlaylist(api, playlistId, fetchArtists = True, fetchTrackFeatures = True):
    tracks = [ t.track for t in api.getPlaylistTracks(playlistId) ]
    trackIds = [ t.id for t in tracks ]

    ### ADD TRACK AUDIO FEATURES ###
    if fetchTrackFeatures:
        features = api.getAudioFeatures(trackIds)
        for (t, f) in zip(tracks, features):
            if t.id != f.id: raise Exception('track and feature ids dont match')
            t.features = f

    ### FILL IN ARTIST INFO ###
    if fetchArtists:
        artistIds = { a.id for a in sum([ t.artists for t in tracks ], []) }
        artists = api.getArtists(list(artistIds))
        artistMap = { a.id: a for a in artists }
        for t in tracks:
            t.artists = [ artistMap[a.id] for a in t.artists ]
    return tracks

def transformPlaylist(api, playlistId, destId, transform, append=True, **kwargs):
    tracks = transform(fetchPlaylist(api, playlistId, **kwargs))
    if not append: api.replacePlaylistTracks(destId)
    #for t in tracks: printTrack(t)
    api.addTracks(destId, [ t.uri for t in tracks ])
