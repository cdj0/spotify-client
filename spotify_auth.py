import sys
import jsonfile
import requests
import oauth

class SpotifyOAuth(oauth.OAuth):
    def __init__(self, configFile, authFile):
        super().__init__(configFile, authFile)

    def getToken(self):
        authData = super().getToken()
        if 'code' in authData:
            authData = self.getUserToken(authData['code'])
            self.updateAuthData(authData)
        return authData

    def getTokenInner(self, params):
        res = requests.post(self.getTokenEndpoint(), data = params,
                headers = { 'Authorization': 'Basic ' + self.getHttpBasicAuth() })
        if res.status_code != requests.codes.ok:
                raise Exception('unepxected status code: ' + str(res.status_code))
        return res.json()

    def getUserToken(self, code):
        userTokenParams = {
                'grant_type': 'authorization_code',
                'code': code,
                'redirect_uri': self.redirectUri() }
        userToken = self.getTokenInner(userTokenParams)
        print(jsonfile.pprint(userToken))
        return userToken

    def getClientToken(self):
        clientTokenParams = { 'grant_type': 'client_credentials' }
        clientToken = self.getTokenInner(clientTokenParams)
        print(jsonfile.pprint(clientToken))
        return clientToken

    def refresh(self):
        url = self.getTokenEndpoint()
        data = {
                'grant_type': 'refresh_token',
                'refresh_token': self.getRefreshToken() }
        self.mergeAuthData(self.getTokenInner(data))

def main():
    import argparse
    parser = argparse.ArgumentParser(description = "authenticate spotify user via oauth")
    parser.add_argument('configFile', help="oauth config file")
    parser.add_argument('tokenFile', help="token storage file")
    args = parser.parse_args()
    auth = SpotifyOAuth(args.configFile, args.tokenFile)
    if auth.getAccessToken():
        auth.refresh()
    else:
        auth.getToken()

if __name__ == "__main__": main()
