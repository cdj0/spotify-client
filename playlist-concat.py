#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify

def append(api, playlistId, appendPlaylists):
    trackUris = sum([ [ t.track.uri for t in api.getPlaylistTracks(pid) ] for pid in appendPlaylists ], [])
    api.addTracks(playlistId, trackUris)

def main():
    parser = argparse.ArgumentParser(description = "concat playlists and append to destination playlist")
    parser.add_argument('destPlaylist', help="destination playlist id")
    parser.add_argument('srcPlaylist', nargs='+', help="one or more source playlists to concat")
    args = parser.parse_args()
    append(initSpotify(), args.destPlaylist, args.srcPlaylist)

main()
