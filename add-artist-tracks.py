#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify

def main():
    parser = argparse.ArgumentParser(description = "add all of an artist's tracks to playlist")
    parser.add_argument('playlist', help="destination playlist id")
    parser.add_argument('artist', help="artist id")
    parser.add_argument('-t', '--album-types', dest="album_types", help="comma separated list of album types: single, album, compilation or appears_on")
    args = parser.parse_args()

    api = initSpotify()
    albums = api.artistAlbums(args.artist, args.album_types)
    trackuris = []
    for album in albums:
        for track in api.albumTracks(album.id):
            trackuris.append(track.uri)
            print('{0} - {1}'.format(track.id, track.name))
    api.addTracks(args.playlist, trackuris)

main()
