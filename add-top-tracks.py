#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify

def addTopTracks(api, playlistId, artistIds):
    tracks = [ track.uri for aid in artistIds for track in api.topTracks(aid).tracks ]
    api.addTracks(playlistId, tracks)

def main():
    parser = argparse.ArgumentParser(description = "add artist's top tracks to playlist")
    parser.add_argument('playlist', help="playlist to add tracks to")
    parser.add_argument('artist', nargs='+', help="artist id")
    args = parser.parse_args()
    addTopTracks(initSpotify(), args.playlist, args.artist)

main()
