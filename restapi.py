import requests
import sys
import time

LOGGING = False
MAX_RETRY_WAIT_MS = 60

def log(*msg, **kwargs):
    print(*msg, file=sys.stderr, **kwargs)

def unexpectedResponse(res):
    log('UNEXPECTED RESPONSE')
    log('URL: ' + res.url)
    log('body:\n' + res.text)
    raise RestException('unexpected status code: ' + str(res.status_code))

def removeStart(s, pre):
    if s.startswith(pre):
        return s[len(pre):]
    return s

def removeEnd(s, post):
    if s.endswith(post):
        return s[:-len(post)]
    return s

def pathJoin(baseDir, path):
    return '{0}/{1}'.format(removeEnd(baseDir, '/'), removeStart(path, '/'))

class JSON:
    def __init__(self, _dict):
        super().__setattr__('_dict', _dict)

    def __getattr__(self, attr):
        if attr in self._dict: return self._dict[attr]
        raise AttributeError('JSON object has no attribute named "{0}"'.format(attr))
    def __setattr__(self, attr, val):
        self._dict[attr] = val

    def __repr__(self):
        return repr(self._dict)

    def __str__(self):
        return str(self._dict)



class RestException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class ServerException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class UnauthorizedException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class RestApi:
    def __init__(self, auth, endpoint, refreshEnabled = True, doLogging = LOGGING):
        self.auth = auth
        self.endpoint = endpoint
        self.refreshEnabled = refreshEnabled
        self.doLogging = doLogging

    def log(self, *msg, **kwargs):
        if self.doLogging: log(*msg, **kwargs)

    def resourcePath(self, resource, id, subpath = None):
        path = '{0}/{1}'.format(resource, id)
        if subpath: path += '/' + subpath
        return path


    def mkUrl(self, path):
        return pathJoin(self.endpoint, path)

    def defaultHeaders(self):
        return {
                'Authorization': 'Bearer ' + self.auth.getAccessToken(),
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=utf-8' }

    def handleResponse(self, res):
        code = res.status_code
        hook = lambda m: JSON(m)
        if code >= 200 and code < 300:
            return None if code == requests.codes.no_content else res.json(object_hook = hook)
        if res.status_code == requests.codes.unauthorized:
            log(f'unauthorized response');
            raise UnauthorizedException()
        if code >= 500 and code < 600:
            log(f'unexpected response: {code}')
            raise ServerException()
        unexpectedResponse(res)

    def request(self, method, path, params = None, json = None):
        doRefresh = self.refreshEnabled
        retryTime = 1
        url = self.mkUrl(path)
        def handleError(e):
                if retryTime > MAX_RETRY_WAIT:
                    raise e
                log(f'retrying after waiting {retryTime}s')
                time.sleep(retryTime)
                retryTime <<= 1

        while True:
            h = self.defaultHeaders()
            res = requests.request(method, url, headers = h, params = params, json = json)
            self.log("{0}: {1} {2}".format(res.status_code, res.request.method, res.url))
            try:
                return self.handleResponse(res)
            except UnauthorizedException as e:
                if not doRefresh:
                    raise e
                doRefresh = False
                self.auth.refresh()
            except ServerException as e:
                handleError(e)
            except requests.exceptions.ConnectionError as e:
                handleError(e)

    def get(self, path, params = None):
        return self.request('GET', path, params = params)

    def patch(self, path, json):
        return self.request('PATCH', path, json = json)

    def post(self, path, json):
        return self.request('POST', path, json = json)

    def put(self, path, json):
        return self.request('PUT', path, json = json)
