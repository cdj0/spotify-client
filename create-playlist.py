#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify

def main():
    parser = argparse.ArgumentParser(description = "create a new playlist")
    parser.add_argument('name', help="name of the new playlist")
    args = parser.parse_args()

    api = initSpotify()
    print(api.createPlaylist(args.name).uri)

main()
