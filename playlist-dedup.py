#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify


def dedup(api, srcPlaylist, destPlaylist, append):
    tracks = list(api.getPlaylistTracks(srcPlaylist))
    numSrcTracks = len(tracks)
    uris = []
    uniq = set()
    for t in tracks:
        id = t.track.id
        if id not in uniq:
            uniq.add(id)
            uris.append(t.track.uri)
        else:
            print('duplicate tracks ', t.track.name)
    numDestTracks = len(uris)
    print(f"Removing {numSrcTracks-numDestTracks} tracks out of {numSrcTracks}")
    if not append: api.replacePlaylistTracks(destPlaylist)
    api.addTracks(destPlaylist, uris)

def main():
    parser = argparse.ArgumentParser(description = "filter playlist tracks")
    parser.add_argument('srcPlaylist', help="source playlist id")
    parser.add_argument('-d', '--dest', dest="destPlaylist", help="destination playlist id")
    parser.add_argument('-a', '--append', action='store_true', help="append to destination playlist")
    args = parser.parse_args()
    if not args.destPlaylist: args.destPlaylist = args.srcPlaylist
    api = initSpotify()
    dedup(api, args.srcPlaylist, args.destPlaylist, args.append)

main()
