#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify

def main():
    parser = argparse.ArgumentParser(description = "remove all tracks from a playlist")
    parser.add_argument('playlist', help="destination playlist id")
    args = parser.parse_args()
    api = initSpotify()
    print(str(api.replacePlaylistTracks(args.playlist)))

main()
