#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify
from spotifytools import fetchPlaylist

def playlistShow(api, playlistId, trackFmt):
    trackFmt = compile(trackFmt, '<format_expression>', 'eval')
    for track in fetchPlaylist(api, playlistId):
        print(eval(trackFmt))

def main():
    parser = argparse.ArgumentParser(description = "show playlist tracks")
    parser.add_argument('playlist', help="playlist id")
    parser.add_argument('format', nargs="?", default="track.uri", help="format expression")
    args = parser.parse_args()

    api = initSpotify()
    playlistShow(api, args.playlist, args.format)


main()

