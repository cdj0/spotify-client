#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify
from spotifytools import playlistFilter


def main():
    parser = argparse.ArgumentParser(description = "filter playlist tracks")
    parser.add_argument('srcPlaylist', help="source playlist id")
    parser.add_argument('destPlaylist', help="destination playlist id")
    parser.add_argument('filterExpression', help="track filter expression")
    args = parser.parse_args()
    api = initSpotify()
    filterExpression = compile(args.filterExpression, '<filter_string>', 'eval')
    playlistFilter(api, args.srcPlaylist, args.destPlaylist, lambda track: eval(filterExpression))

main()
