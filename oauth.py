from http.server import BaseHTTPRequestHandler, HTTPServer
import webbrowser
import jsonfile
from urllib.parse import urlencode
from urllib.parse import parse_qsl
from _thread import start_new_thread
from base64 import b64encode

class OauthHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        with open('oauth.html', 'rb') as f:
            self.wfile.write(f.read())

    def do_HEAD(self):
        self._set_headers()

    def do_POST(self):
        # Doesn't do anything with posted data
        global _oauthInfo
        self._set_headers()
        _oauthInfo = parseQueryString(self.path)
        def killServer(server):
            server.shutdown()
        start_new_thread(killServer, (self.server,))

class OAuth:
    def __init__(self, configFile, authFile):
        self.config = jsonfile.load(configFile)
        self.authFile = authFile
        try:
            self.authData = jsonfile.load(authFile)
        except Exception as e:
            self.authData = None

    def getToken(self):
        if self.authData == None or self.getAccessToken() == None:
            self.openBrowserAuth()
            self.runServer()
            return self.updateAuthData(_oauthInfo)
        return self.authData

    def updateAuthData(self, authData):
        self.authData = authData
        self.save()
        return self.authData

    def mergeAuthData(self, authData):
        for k in authData:
            self.authData[k] = authData[k]
        self.save()

    def save(self):
        jsonfile.save(self.authFile, self.authData)

    def openBrowserAuth(self):
        params = {
                'client_id': self.getClientId(),
                'redirect_uri': self.redirectUri() }
        for k in self.config['params']:
            params[k] = self.config['params'][k]
        url = self.config['oauthEndpoint'] + queryString(params)
        print('redirecting to ' + url)
        webbrowser.open(url)

    def redirectUri(self):
        return 'http://localhost:' + str(self.config['listenPort']) + self.config['redirectPath']

    def refresh(self):
        raise Exception('unimplemented')

    def getTokenEndpoint(self):
        return self.config['tokenEndpoint']

    def getAccessToken(self):
        return self.authData['access_token'] if self.authData and 'access_token' in self.authData else None

    def getRefreshToken(self):
        return self.authData['refresh_token']

    def getClientId(self):
        return self.config['clientId']

    def getClientSecret(self):
        return self.config['clientSecret']

    def getHttpBasicAuth(self):
        return bytes.decode(b64encode(str.encode(self.getClientId() + ':' + self.getClientSecret())))

    def runServer(self, server_class = HTTPServer, handler_class = OauthHandler):
        port = self.config['listenPort']
        server_address = ('', port)
        httpd = server_class(server_address, handler_class)
        httpd.serve_forever()
        httpd.server_close()



def queryString(params):
    return '' if len(params) == 0 else '?' + urlencode(params)

def parseQueryString(path):
    parts = path.split('?', 2)
    params = {}
    if len(parts) < 2:
        return params
    for key, val in parse_qsl(parts[1]):
        params[key] = val
    return params

def main():
    import argparse
    parser = argparse.ArgumentParser(description = "authenticate user via oauth")
    parser.add_argument('configFile', help="oauth configuration file")
    parser.add_argument('tokenFile', help="file for storing token info")
    args = parser.parse_args()

    auth = OAuth(args.configFile, args.tokenFile)
    auth.getToken()

if __name__ == "__main__":
    main()

