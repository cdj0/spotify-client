#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify

def artistNames(track):
    return ', '.join([ a.name for a in track.artists ])


def main():
    parser = argparse.ArgumentParser(description = "show users top tracks or artists")
    parser.add_argument('type', choices=['tracks', 'artists'], nargs="?", default="artists", help="type of request")
    parser.add_argument('timeRange', choices=['short', 'medium', 'long'], nargs="?", default="medium", help="time range")
    args = parser.parse_args()

    api = initSpotify()
    topItems = api.topItems(args.type, args.timeRange + "_term")
    for item in topItems:
        if item.type == 'track':
            print('{0} {1} - {2}'.format(item.uri, artistNames(item), item.name))
        else:
            print('{0} {1}'.format(item.uri, item.name))

main()
