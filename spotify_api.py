import jsonfile
from restapi import RestApi
from spotify_auth import SpotifyOAuth
import itertools

SPOTIFY_ENDPOINT = 'https://api.spotify.com/v1/'

def grouper(iterable, n):
    it = iter(iterable)
    while True:
       chunk = list(itertools.islice(it, n))
       if not chunk:
           return
       yield chunk

class SpotifyException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class Spotify(RestApi):
    def __init__(self, auth):
        super().__init__(auth, SPOTIFY_ENDPOINT)
        self.userInfo = None

    def artistPath(self, artistId, subpath = None):
        return self.resourcePath('artists', artistId, subpath)

    def albumPath(self, albumId, subpath = None):
        return self.resourcePath('albums', albumId, subpath)

    def playlistPath(self, playlistId, subpath = None):
        return self.resourcePath('playlists', playlistId, subpath)

    def userPath(self, userId, subpath = None):
        return self.resourcePath('users', userId, subpath)

    ## TODO get top N results with batching
    def queryArtists(self, artist):
        params = {
                'q': 'artist:' + artist,
                'type': 'artist',
                'market': 'from_token',
                'offset': '0',
                'limit': '1' }
        return self.get('search', params = params)

    def queryArtist(self, artist):
        return self.queryArtists(artist).artists.items[0]

    def relatedArtists(self, artistId):
        return self.get(self.artistPath(artistId, 'related-artists'))

    def getAll(self, path, params = {}, offset = 0, batchsize = 20):
        result = []
        offset = 0
        total = offset + 1
        while offset < total:
            params['offset'] = offset
            params['limit'] = batchsize
            res = self.get(path, params = params)
            items = res.items
            for item in items: yield item
            offset += len(items)
            total = res.total

    def artistAlbums(self, artistId, includeGroups = None):
        p = { 'include_groups': includeGroups } if includeGroups else {}
        return self.getAll(self.artistPath(artistId, 'albums'), params = p, batchsize = 50)

    def albumTracks(self, albumId):
        return self.getAll(self.albumPath(albumId, 'tracks'), batchsize = 50)

    def topTracks(self, artistId):
        params = { 'market': 'from_token' }
        return self.get(self.artistPath(artistId, 'top-tracks'), params = params)

    def addTracks(self, playlistId, trackUris):
        batchsize = 100
        path = self.playlistPath(playlistId) + '/tracks'
        for chunkUris in grouper(trackUris, batchsize):
            self.post(path, json = {'uris': chunkUris})

    def getPlaylistTracks(self, playlistId):
        return self.getAll(self.playlistPath(playlistId, 'tracks'), batchsize = 100)

    def getArtists(self, artistIds, batchsize=50):
        return itertools.chain.from_iterable((self.get('artists', params = dict(ids = ','.join(batch))).artists for batch in grouper(artistIds, batchsize)))

    def getAudioFeatures(self, trackIds, batchsize=100):
        return itertools.chain.from_iterable((self.get('audio-features', params=dict(ids=','.join(batch))).audio_features for batch in grouper(trackIds, batchsize)))

    def getUserInfo(self):
        if not self.userInfo: self.userInfo = self.get('me')
        return self.userInfo

    def getUserId(self):
        return self.getUserInfo().id

    def createPlaylist(self, name, public = False, collab = False):
        userId = self.getUserId()
        body = { 'name': name, 'public': public, 'collaborative': collab }
        return self.post(self.userPath(userId, 'playlists'), json = body)

    def replacePlaylistTracks(self, playlistId, trackUris = []):
        if len(trackUris) > 100:
            raise SpotifyException('Too many tracks: ' + len(trackUris))
        body = { 'uris': trackUris }
        return self.put(self.playlistPath(playlistId,'tracks'), json = body)

    def topItems(self, topType = 'artists', timeRange = 'medium_term'):
        p = { 'time_range': timeRange }
        return self.getAll('me/top/' + topType, params = p, batchsize = 50)


def initSpotify(configFile = 'config.json'):
    config = jsonfile.load(configFile)
    auth = SpotifyOAuth(config['oauthConfig'], config['tokenFile'])
    auth.getToken()
    return Spotify(auth)
