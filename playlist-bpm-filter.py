#!/usr/bin/env python3

import argparse
from spotify_api import initSpotify
from spotifytools import playlistFilter

def bpmFilter(api, playlistId, bpmPlaylistId, bpm, tolerance):
    def trackFilter(t):
        trackFeatures = t.features
        trackBpm = trackFeatures.tempo
        return (abs(bpm - trackBpm) < tolerance) or (abs(bpm - (trackBpm * 2.0)) < tolerance)

    playlistFilter(api, playlistId, bpmPlaylistId, trackFilter, fetchArtists = False)


def main():
    parser = argparse.ArgumentParser(description = "filter playlist by beats per minute")
    parser.add_argument('srcPlaylist', help="source playlist id")
    parser.add_argument('destPlaylist', help="destination playlist id")
    parser.add_argument('bpm', type=float, help="target beats per minute")
    parser.add_argument('-t', '--tolerance', type=float, help="tolerance in beats per minute")
    args = parser.parse_args()

    api = initSpotify()
    bpmFilter(api, args.srcPlaylist, args.destPlaylist, args.bpm, args.tolerance)

main()
