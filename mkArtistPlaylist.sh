#!/bin/bash

if [[ $# -lt 3 ]]; then
	echo "USAGE $0 <playlistName> <artists...> <depth>"
	exit 1
fi

toid() { printf "${1##*:}"; }

playlistName=$1
shift

artists=()
while [[ $# -gt 1 ]]; do
	artists=("${artists[@]}" "$1")
	shift
done
depth="$1"

playlistId=$(toid $(./create-playlist.py "$playlistName"))

echo "Finding related artists..."
for a in "${artists[@]}"; do
	echo "Finding artists related to $a" >&2
	./related-artists.py "$a" "$depth"
done | sort -u | sed 's/ .*//' | xargs ./add-top-tracks.py "$playlistId"

echo "Sorting..."
./playlist-sort.py "$playlistId" "track.album.release_date" "-track.artists[0].popularity" "-track.popularity"

echo "Checking for duplicates..."
./playlist-dedup.py "$playlistId"
